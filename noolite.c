#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <poll.h>
#include <fcntl.h>
#include <sys/types.h>
#include <termios.h>
#include <errno.h>
#include <pthread.h>
#include <syslog.h>


#include "domovoy_conf.h"


int uart = -1, noo_mosi = -1 , noo_miso = -1;


void create_noolite_message(uint8_t * msg,  uint8_t mode, uint8_t ctr, 
                            uint8_t chanel, uint8_t cmd, uint8_t * data)
{
    int i;
    msg[0] = 0xAB;
    msg[1] = mode;
    msg[2] = ctr;
    msg[3] = 0;
    msg[4] = chanel;
    msg[5] = cmd;
    for(i = 6; i < 11; i++) 
        msg[i] = data[i - 6];
    for(i = 11; i < 15; i++)
        msg[i] = 0;

    uint8_t crc = 0;
    for(i = 0; i < 15; i++)
        crc += msg[i];
    msg[15] = crc; 
    msg[16] = 0xAC;
} 


void process_answer(uint8_t * answer)
{
    if (answer[1] != 0x01)  
        return;
    int i;
    uint8_t crc = 0;
    for(i = 0; i < 15; i++)
        crc += answer[i];
    if (crc != answer[15])
        return;
    uint8_t info_msg[256];
    switch (answer[5] ) {
        case 0:
        case 2:
            info_msg[0] = 4;
            info_msg[1] = MESSAGE_TYPE_INFO;
            info_msg[2] = MESSAGE_TYPE_INFO_DATA;
            info_msg[3] = answer[5];
            info_msg[4] = answer[4];
            write(noo_miso, info_msg, 5);
            syslog(LOG_DEBUG, "Recive command from button");
            break;
        default:
            return;
    }
}

void  * read_answer(void * arg)
{
    uint8_t answer[20]; 
    int i = 0;
    uint8_t c;
    struct pollfd fds[1];
    
    fds[0].events =  POLLIN;
    while(1) {
	
        fds[0].fd = uart;
        if(poll(fds, 1, 100) > 0) {
            if (read(uart, &c, 1 )) {
				switch(i) {
					case 0:
						if((c == 0xAD)){
							answer[0] = c;
							i++; 
						}
						break;
					case 16:
						answer[16] = c;
						process_answer(answer);
						i = 0;
						break;
					default:
						answer[i] = c;
						i++; 
						break;							
				}
            }
        }
    }
}




int connect_to_domovoy_system () { 
    char path[512];
    syslog(LOG_DEBUG, "Open fifo noolite_mosi");
    if(noo_mosi < 0){
        sprintf(path, "%s/%s%s", PIPE_PATH, NOOLITE_PIPE, "_mosi");
        noo_mosi = open(path, O_RDONLY );
        if (noo_mosi < 0) {
            syslog(LOG_ERR, "Cant open pipe to Domovoy subsystem");
            return 1;
        }
    }
    sprintf(path, "%s/%s%s", PIPE_PATH, NOOLITE_PIPE, "_miso");
 
    noo_miso = open(path,  O_WRONLY);
    if (noo_mosi < 0) {
        
        syslog(LOG_ERR, "Cant open pipe from Domovoy subsystem");
        return  1;
    }
    return 0 ;
}

void close_everything_and_exit () { 
    if(noo_mosi >= 0){
        close(noo_mosi);
    }
    if(noo_miso >= 0){
        close(noo_miso);
    }
    if(uart >= 0){
        close(uart);
    }
    exit(1);    
}

void process_config_command(uint8_t len, char * cmd) {
    syslog(LOG_DEBUG, "Config command recieved");
    
    switch (cmd[0]){
        case MESSAGE_TYPE_CONFIG_TTY_ADRESS:
            if (uart >= 0)
                close(uart);
            uart = open(cmd + 1, O_RDWR | O_NOCTTY | O_NONBLOCK);
            if(uart <= 0){
                syslog(LOG_ERR, "ERROR: Can't open uart PORT for NooLIte system");
                close_everything_and_exit();
            }
            struct termios newtio;
            bzero(&newtio, sizeof(newtio));
            newtio.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
            newtio.c_iflag = IGNPAR;
            newtio.c_oflag = 0;
            newtio.c_lflag = 0;
            newtio.c_cc[VTIME]    = 0;   
            newtio.c_cc[VMIN]     = 1;   
            tcflush(uart, TCIFLUSH);
            tcsetattr(uart,TCSANOW,&newtio);
            break;
        default:
            syslog(LOG_ERR, "Unknown config command");
            break;

    }
}


void process_action_command(uint8_t len, char * cmd) {
    uint8_t cmd_out[17];
    uint8_t data[5];
    uint8_t fl = 1;
    for (int i = 0; i < 5; i++)
        data[i] = 0;

    switch (cmd[0]) {
        case 0:
            create_noolite_message(cmd_out, 0 , 0, cmd[1], 0,data);
            break;
        case 2:
            create_noolite_message(cmd_out, 0 , 0, cmd[1], 2,data);
            break;
        default:
            syslog(LOG_ERR, "unknown command");
            return;
    }
    
    write(uart, cmd_out, 17);
}



void main(int argc, char ** argv)
{
    openlog("Noolite", 0 , LOG_LOCAL0);
    syslog(LOG_DEBUG, "Starting noolite");
    
//    start_log(LOG_FOLDER, LOG_FILE, ERROR_LOG_FILE);
    syslog(LOG_DEBUG, "Connecting to domovoy system");
    while(connect_to_domovoy_system())
    {
        sleep(5);   
    }
    syslog(LOG_DEBUG, "Connected!");
    
    pthread_t thread;
    syslog(LOG_DEBUG, "Creating second thread...");
    int rez = pthread_create(&thread, NULL, read_answer, NULL);
    if (rez)
    {
         syslog(LOG_ERR, "Can't create thread for listening Noolite");
         close_everything_and_exit();
    }
    syslog(LOG_DEBUG, "Succes!");
    struct pollfd fds[1];    
    fds[0].events =  POLLIN;
    fds[0].fd = noo_mosi;
    uint8_t buf[255];
    uint8_t c;
    syslog(LOG_DEBUG, "Starting main loop");
    
    while(1) {
        if(poll(fds, 1, -1) > 0) {
            syslog(LOG_DEBUG, "Command received");
            if(read(noo_mosi, &c, 1)){
                if(read(noo_mosi, buf, c) == c) {
                    switch (buf[0]){
                        case MESSAGE_TYPE_ACTION:
                            process_action_command(c - 1, buf + 1);
                            break;
                        case MESSAGE_TYPE_CONFIG:
                            process_config_command(c - 1, buf + 1);
                            break;
                        default:
                            syslog(LOG_ERR, "Unknown message type");
                            break;
                    }
                }
                else{
                    syslog(LOG_ERR, "Wrong message format from domovoy system");
                }
            }
        }
    }
}
