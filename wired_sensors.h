#include <stdio.h>
#include <stdint.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>
#include <semaphore.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/time.h>

#include "domovoy_conf.h"

#define LIST_OF_CONNECTED_DEVICES "/usr/local/src/domovoy_list_of_devices"
#define SETTINGS_FILE_PATH "/usr/local/src/domovoy_settings"
#define LIST_OF_AVAILABLE_ADDRESSES "/usr/local/src/list_of_addresses"
#define RS485_PORT "/dev/ttyS2"

#define VALID 1
#define UNVALID 0

#define NEW_DEVICE_ADDR 0
#define DEVISE_NOT_REGISTERED -1
#define MAX_DEVICE_COUNT 30

//Types of connected devices
#define HUB 1 
#define UNEVERSAL_MODULE 10
#define WATER_FLOW_CONTROLLER 11
#define HEATING_CONTROLLER 12

//Type of connected modules
#define TEMPERATURE_LM75 0b00000001
#define TEMPERATURE_AND_HUMIDITY_SHT30 0b00000010
#define ILLUMINATION_KPS_3227 0b00000100
#define ADC_INPUT_PLATE 0b00001000
#define SAFETY_SENSORS_INPUT 0b00010000
#define RELAY_OUTPUT 0b00100000

//Safety sansors types
#define SMOKE_SENSOR 0b0001
#define MOTION_SENSOR 0b0010
#define FLUID_SENSOR 0b0100
#define DOOR_SENSOR 0b1000

//Available uart statuses
#define IDLE 0
#define WAITING_FOR_ANSWER 1

//Device states
#define ONLINE 1
#define OFFLINE 0

//Requests
#define REQUEST_FOR_DEVICES_WITHOUT_ADDRESS 0x22
#define REQUEST_FACTORY_PARAM 0x24
#define REQUEST_NOW_STATE 0x21
//Commands
#define COMMAND_SET_NEW_ADDRESS 0x8A
#define COMMAND_TOOGLE_ENERGY_SAVING_MODE_ON 0x12 
#define COMMAND_TOOGLE_ENERGY_SAVING_MODE_OFF 0x13
#define COMMAND_SAVE_ADC_VALUE 0x11
#define COMMAND_RESET 0x04
#define COMMAND_BLINK_ON 0x0E
#define COMMAND_BLINK_OFF 0x0F
#define COMMAND_NEW_PARAM 0x8C
#define COMMAND_SET_DEVICE_TYPE 0x94
#define COMMAND_WRITE_EEPROM 0x8D
#define COMMAND_READ_EEPROM 0x8E
//Answers
#define RESPONSE_NEW_ADDRESS 0xCA
#define RESPONSE_NEW_DEVICE 0x62
#define RESPONSE_FACTORY_PARAM 0xE4
#define RESPONSE_TOOGLE_ENERGY_SAVING_MODE_ON 0x52
#define RESPONSE_TOOGLE_ENERGY_SAVING_MODE_OFF 0x53
#define RESPONSE_CURRENT_STATE 0xE1
#define RESPONSE_SAVE_ADC_VALUE 0x51
#define RESPONSE_RESET 0x44
#define RESPONSE_BLINK_OFF 0x4F
#define RESPONSE_BLINK_ON 0x4E
#define RESPONSE_DEVISE_WITHOUT_ADDRESS 0x62
#define RESPONSE_NEW_PARAM_RESPONSE 0x4C
#define RESPONSE_NEW_PARAM_ANSWER 0xCC
#define RESPONSE_SET_DEVICE_TYPE 0x54
#define RESPONSE_COMMAND_NOT_DONE_ERROR 0x4A
#define RESPONSE_WRITE_EEPROM 0xCD
#define RESPONSE_READ_EEPROM 0xCE

//Settings names
#define REQUEST_SPEED "Request_speed"

#define COMMON_TIMEOUT 3
#define LONG_TIMEOUT 500

//input chanell states
#define STATE_SHORT_CIRCUIT 1
#define STATE_OPEN 2
#define STATE_CHANGE 4
#define STATE_IDLE 0 

//Relay states
#define RELAY_STATE_OFF 0
#define RELAY_STATE_ON 1
#define RELAY_STATE_DO_NOTHING 2

// Reddis msg types
#define RELAY 0
#define SENSOR 1

//Relay answer info
#define RELAY_ANSWER_MASK 0b00000011
#define FIRST_ERR 1
#define SECOND_ERR 2
#define BOTH_ERR 3
#define BOTH_OK 0

//Should be sent flags
#define DEFAULT_STATE 0b00000001
#define SAFETY_SATE 0b00000010

#define REGISTERED 1
#define NOT_REGISTERED 0
//******************* Messages format ***************************

// (Id,state,value)

//***************************************************************



typedef struct {
    
    float temperatureSHT30;
    float humiditySHT30;
    float temperatureLM75;
    uint8_t illuminationKPS3227;
    uint8_t adcOne;
    uint8_t adcTwo;
    uint8_t adcThree;
    uint8_t countOfChangesOnFirstChanel;
    uint8_t countOfChangesOnSecondChanel;
    uint8_t chanelOneState;
    uint8_t chanelTwoState;

}DecodedData;

typedef struct {
    uint8_t command;
    uint8_t data[20];
    int dataLen;
} Message;

typedef struct {
    char type[10]; 
    int number;
    char func[10];
    char fitstArg[10];
    char secondArg[10];
} MessageFromServer;

typedef struct info_device_all_slave
{
    /*unsigned available          :1;  //наличие устройства на линии (при опросе всех адресов “кто на линии?”)
				                    //для составления списка активных адресов
    unsigned speed_request  :1;  //требуемая скорость опроса
    unsigned rx_err               :1;  //результат последнего опроса (получен корректный ответ или нет)
    unsigned opros_yes        :1;  //опрос выбранного устройства в текущем цикле прошёл
    unsigned device_online  :1; //если слэйв не отвечает более 5 раз подряд - ставиться бит “0” 
       //(устр-ва нет на линии) 
    unsigned temp                :3; //резерв
    uint8_t address;                //адрес устройства
    uint8_t type_device;            //тип устройства*/
    uint8_t deviceTypeHi;        //тип данного устройства (главный класс устр-ва - универс. устр-во, протечка, отопление)
    uint8_t deviceTypeLo;        //подтип данного устройства (подкласс устр-ва - что распаяно на плате: темп, темп+влаж, освещённость, входы без-ти и выходы реле)
    uint8_t deviceSensorsType;           //тип подключенных датчиков безопасности (объёмники, герконы, пожарка и пр.)
    uint8_t deviceVersionHi;                 //версия устройства (ст.)
    uint8_t deviceVersionLo;                 //версия устройства (мл.)
    uint8_t countOfBytesInStateAnswer;     //кол-во байт полезной инфы передаваемой в ответе на запрос состояния
    uint8_t countOfAccecableOutPorts;            //кол-во настраиваемых выходных портов
    uint16_t countOfErrorsInMessagesFromMaster;        //счётчик ошибок приёма данных от мастера (считается и храниться слэйвом)
    uint32_t countOfErrorsInMessagesFromDevice;        //счётчик ошибок приёма данных от слэйва (считается и храниться мастером)
    uint8_t deviceAddress;
    uint8_t deviceStatus;       //Online or offline
    uint8_t deivceData[20];
    uint8_t shouldBeSent;
    _Bool registered;
    char firstSensorType[10];
    char secondSensorType[10];
    DecodedData PreviousData;
    DecodedData CurentData;
    Message NextMessage;

}tinfo_device_all_slave;


uint8_t crc8(uint8_t * msg, int count);

int saveDeviceData(tinfo_device_all_slave * Device);

int readSavedDeviceData();

int setTimedSem();

int sendMessageToSensor(uint8_t address, uint8_t command, uint8_t * buf, int bufLen, int timeout);

int readFromUart(int fd, void * buf, int lenOfDataToRead);

int readFullMessage(int fd, uint8_t * buf);

int checkMessage(uint8_t * buf, int buflen);

int chooseNewAddress();

int identifySender(uint8_t senderAddress);

void handleMessage(uint8_t  message[], int messageLenght);

void * uartReader( void * arg);

void * read_uart( void * arg);

static uint8_t Crc8Table[256]  = 
    {
        0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97,
        0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E,
        0x43, 0x72, 0x21, 0x10, 0x87, 0xB6, 0xE5, 0xD4,
        0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D,
        0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11,
        0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8,
        0xC5, 0xF4, 0xA7, 0x96, 0x01, 0x30, 0x63, 0x52,
        0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB,
        0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 0x9B, 0xAA,
        0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13,
        0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9,
        0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50,
        0xBB, 0x8A, 0xD9, 0xE8, 0x7F, 0x4E, 0x1D, 0x2C,
        0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95,
        0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F,
        0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6,
        0x7A, 0x4B, 0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED,
        0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54,
        0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 0x9F, 0xAE,
        0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17,
        0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B,
        0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2,
        0xBF, 0x8E, 0xDD, 0xEC, 0x7B, 0x4A, 0x19, 0x28,
        0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91,
        0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0,
        0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69,
        0x04, 0x35, 0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93,
        0xBD, 0x8C, 0xDF, 0xEE, 0x79, 0x48, 0x1B, 0x2A,
        0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 0x67, 0x56,
        0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF,
        0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15,
        0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC
    };