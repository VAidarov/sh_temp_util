#ifndef _SHT3_H_
#define _SHT3_H_


#include <stdint.h>

float sht3GetTemp(uint8_t * buf);
float sht3GetHumidity(uint8_t * buf);



#endif