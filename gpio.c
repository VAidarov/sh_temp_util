#include "gpio.h"

int pinInit(int pin, int direction){
    int fd;
    char path[100] = {0}, buf[5] = {0}; 
    sprintf(path, GPIO_PATH GPIO_VAl, pin);
    fd = open(path, O_WRONLY);
    return fd;
}

void pinOff(int fd){
    int  len;
    len = write(fd, "0" , 1);
    close(fd);
}


int pinState(int pin, int state, int fd){
    char path[100] = {0}, buf = 0;
    int len;
    if (state > 1){
        state = 1;
    }
    buf = state + '0';
    len = write(fd, &buf, 1);
    return len;
}
