
COMPILE_ARGS = -pthread  -std=gnu99
REDIS = hiredis/hiredis.o \
	hiredis/sds.o \
	hiredis/read.o \
	hiredis/net.o \
	


.PHONY: all clean

all: domovoy noolite wired_sensors

clean:
	rm -rf domovoy noolite wired_sensors *.o

wired_sensors: wired_sensors.c wired_sensors.h domovoy_conf.h sht3.h sht3.c gpio.c gpio.h
	gcc $(COMPILE_ARGS) -o wired_sensors wired_sensors.c sht3.c gpio.c

domovoy: domovoy.c domovoy.h domovoy_conf.h hiredis 
	
	gcc $(COMPILE_ARGS) -o domovoy domovoy.c $(REDIS)

hiredis: makeHiRedis.sh
	./makeHiRedis.sh

noolite: noolite.c domovoy_conf.h 
	gcc $(COMPILE_ARGS) -o noolite noolite.c 

install: all
	install noolite /usr/sbin/ &
	install domovoy /usr/sbin/ &
	install wired_sensors /usr/sbin/ &
	chmod ugo+x gpio_init.sh 
	install gpio_init.sh /etc/init.d/ 
	chmod ugo+x hub_init.sh 
	install hub_init.sh /etc/init.d/ 
	update-rc.d gpio_init.sh defaults
	update-rc.d hub_init.sh defaults 
	touch /usr/local/src/domovoy_list_of_devices
	touch /usr/local/src/domovoy_settings
	touch /usr/local/src/list_of_addresses
	echo 3 > /usr/local/src/list_of_addresses