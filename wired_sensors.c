#include "wired_sensors.h"
#include "sht3.h"
#include "gpio.h"

int uartFd, pinFd;
pthread_mutex_t globalBufMutex;
int uartStatus, countOfConnectedDevices = 0, domovoy_ws_miso = -1, domovoy_ws_mosi = -1;
sem_t uartSemaphore,handlerSemaphore;
uint8_t globalBuf[50];

struct timespec TimeOutTime;
struct pollfd fds[1];
tinfo_device_all_slave RegisteredDevices[MAX_DEVICE_COUNT];
MessageFromServer messageFromServer;

uint8_t crc8(uint8_t * msg, int count)
{
    uint8_t crc = 0xFF;
    int i;
    for (i = 0; i < count; i++)
    {
        crc = Crc8Table[crc ^ msg[i]];
    }

    return crc;

}

void binaryToHexStr(uint8_t * buf, int len, char* str)
{
     int p;
    char digits[] = "0123456789ABCDEF";
    for (p = 0; p < len; p++)

    {
        str[p * 3] = digits[(buf[p]) >> 4];
        str[p * 3 + 1] = digits[(buf[p]) & 0xF];
        str[p * 3 + 2] = ' ';
        
    }
    str[len * 3 ] = '\0';
    
}

void setNewMessage(uint8_t command, uint8_t * buf, int buflen, int deviceNum){
    int i;
    RegisteredDevices[deviceNum].NextMessage.command = command;
    memset(RegisteredDevices[deviceNum].NextMessage.data, 0, 20);
    for(i = 0; i < buflen; i++){
        RegisteredDevices[deviceNum].NextMessage.data[i] = buf[i];
    }
    RegisteredDevices[deviceNum].NextMessage.dataLen = buflen;
}

int saveDeviceData(tinfo_device_all_slave * Device){

    FILE * stream = fopen(LIST_OF_CONNECTED_DEVICES, "a+");
    if(stream == 0){
        syslog(LOG_ERR,"Can`t open stream to save new device data | Errno: %d", errno);
        return(-1);
    }
    if(fprintf(stream, "\nDevice_addr: %x", Device->deviceAddress) < 0){
        syslog(LOG_ERR,"Can`t write into file stream | Errno: %d", errno);
        return(-1);
    }
    fclose(stream);
    return(0);
}

int readSavedDeviceData(){
    
    FILE * stream = fopen(LIST_OF_CONNECTED_DEVICES, "rt");
    if(stream == 0){
        syslog(LOG_ERR,"Can`t open stream to read saved device data | Errno: %d", errno);
        return(-1);
    }
    fseek(stream, 0, SEEK_SET);
    while(!feof(stream)){
        int len = fscanf(stream, "\nDevice_addr: %x", &RegisteredDevices[countOfConnectedDevices].deviceAddress); 
        if(len > 0){
            RegisteredDevices[countOfConnectedDevices].registered = REGISTERED;
            setNewMessage(REQUEST_FACTORY_PARAM, NULL, 0, countOfConnectedDevices);
            countOfConnectedDevices++;
        }
        
    }
    fclose(stream); 
}

int setTimedSem(int timeout){
    struct timespec tim; 
    int prescaler = 1000000, maxNsec = 999999999;    
    timeout = timeout * prescaler;
    clock_gettime(1, &tim);
    if(tim.tv_nsec < maxNsec - timeout){
        tim.tv_nsec += timeout;
    }else{
        tim.tv_sec += 1;
        tim.tv_nsec = timeout - (maxNsec - tim.tv_nsec);
    }
    return sem_timedwait(&uartSemaphore, &tim);
}


void sendMessageToDomovoySystem(char * message){
    int len = -1;
    len = write(domovoy_ws_miso, message, strlen(message));
    if(len < 0){
        syslog(LOG_ERR, "Can`t send message to server. errno %d", errno);
    }
}

void parseSafetySensorsTypeFromBytecode(tinfo_device_all_slave * device){
    uint8_t bytecode = device->deviceSensorsType;
    if(bytecode & SMOKE_SENSOR){
        strcpy(device->firstSensorType, "smoke");
    }else if(bytecode & MOTION_SENSOR){
        strcpy(device->firstSensorType, "motion");
    }else if(bytecode & FLUID_SENSOR){
        strcpy(device->firstSensorType, "fluid");
    }else if(bytecode & DOOR_SENSOR){
        strcpy(device->firstSensorType, "door");
    }
    bytecode = bytecode >> 4;
    if(bytecode & SMOKE_SENSOR){
        strcpy(device->firstSensorType, "smoke");
    }else if(bytecode & MOTION_SENSOR){
        strcpy(device->firstSensorType, "motion");
    }else if(bytecode & FLUID_SENSOR){
        strcpy(device->firstSensorType, "fluid");
    }else if(bytecode & DOOR_SENSOR){
        strcpy(device->firstSensorType, "door");
    }
}

uint8_t checkSafetySensorsType(char * buf){
    if(!strncmp(buf, "smoke", 5)){
        return SMOKE_SENSOR;
    }
    if(!strncmp(buf, "motion",6)){
        return MOTION_SENSOR;
    }
    if(!strncmp(buf, "fluid", 5)){
        return FLUID_SENSOR;
    }
    if(!strncmp(buf, "door", 4)){
        return DOOR_SENSOR;
    }
    return 0;
}

int timeToSleep(int num){
    int time_to_sleep;
    time_to_sleep = num * 100000 / 1152;
    return time_to_sleep;
}

int sendMessageToSensor(uint8_t address, uint8_t command, uint8_t * buf, int bufLen, int timeout){
    int i, len, msgLen;
    uint8_t outBuf[50];
    char logBuf[100];
    outBuf[0] = 0x55;
    outBuf[1] = 0xFF;
    outBuf[2] = address;
    outBuf[3] = 0x02;
    outBuf[4] = command;
    outBuf[5] = crc8(outBuf, 5);
    outBuf[6] = 0;
    msgLen = 6;
    if(bufLen > 0){
        outBuf[6] = bufLen;
        for (i = 0; i < bufLen; i++){
            outBuf[i+7] = buf[i];
        }
        outBuf[bufLen + 7] = crc8(buf, bufLen);
        outBuf[bufLen + 8] = 0;
        msgLen += bufLen + 2;
    }
    pinState(3, HIGH,pinFd);
    len = write(uartFd, outBuf, msgLen);
    if(len < 0){
        syslog(LOG_ERR, "Can`t write into UART | Errno: %d", errno);
    }
    usleep(timeToSleep(len));
    pinState(3, LOW, pinFd);
    binaryToHexStr(outBuf, msgLen, logBuf);
    syslog(LOG_DEBUG, "[SENDER] Was sent %s", logBuf);
    return setTimedSem(timeout);

}

int readFromUart(int fd, void * buf, int lenOfDataToRead){
    int len;
    len = read(fd, buf, lenOfDataToRead);
        if(len < 0){
            syslog(LOG_ERR, "Can`t read from uart | Errno: %d", errno);
        }
    return len;
}

int readFullMessage(int fd, uint8_t * buf){
    int len = 1;
    buf[0] = 0xAA;
    len += readFromUart(fd, buf + 1, 5);
    if(buf[4] & 1 << 7){
        len += readFromUart(fd, &buf[6], 1);
        len += readFromUart(fd, buf + 7, buf[6]+1);
    }
    return len;
}

int checkMessage(uint8_t * buf, int buflen){
    _Bool validity = VALID;
    if (buf[1] != 0xFF){
        validity = UNVALID;
    }
    
    if (crc8(buf, 5) != buf[5]){
        validity = UNVALID;
    }
    if (buflen > 6){
        if (crc8(buf + 7, buf[6]) != buf[buflen -1]){
            validity = UNVALID;
        }
    }
    return validity;
}

int chooseNewAddress(){
    FILE * stream;
    int readedValue = 0;
    stream = fopen(LIST_OF_AVAILABLE_ADDRESSES, "r+");
    fscanf(stream, "%x", &readedValue);
    if(readedValue > 255){
        syslog(LOG_ERR, "Reached end of available addresses");
        return 0;
    }
    fseek(stream, 0, SEEK_SET);
    fprintf(stream, "%x ", readedValue+1);
    fclose(stream);
    
    return readedValue;
}

void sortFactoryParam(uint8_t * data, int  len, int num){
    //syslog(LOG_DEBUG, "[HANDLER] Parsing factory params");
    if (len >= 8){
        RegisteredDevices[num].deviceTypeHi = data[0];
        RegisteredDevices[num].deviceTypeLo = data[1];
        RegisteredDevices[num].deviceSensorsType = data[2];
        RegisteredDevices[num].deviceVersionHi = data[3];
        RegisteredDevices[num].deviceVersionLo = data[4];
        RegisteredDevices[num].countOfBytesInStateAnswer = data[5];
        RegisteredDevices[num].countOfAccecableOutPorts = data[6];
        RegisteredDevices[num].countOfErrorsInMessagesFromMaster = data[7]*256;
    }
    if(RegisteredDevices[num].deviceSensorsType != 0){
        parseSafetySensorsTypeFromBytecode(&RegisteredDevices[num]);
    }
    syslog(LOG_DEBUG, "[HANDLER] Was read params Hi type: %x | Lo type: %x", RegisteredDevices[num].deviceTypeHi, RegisteredDevices[num].deviceTypeLo);
}

int readFromSettings(char * paramName){
    FILE * stream;
    char buf[20];
    int paramValue;
    stream = fopen(SETTINGS_FILE_PATH, "rt");
    while(!feof(stream)){
        fscanf(stream, "%s %d",buf, &paramValue);
        if(strncmp(buf, paramName, strlen(paramName)) == 0){
            return paramValue;
        }
    }
}

int identifyDeviceByAddress(uint8_t senderAddress){
    int i;
    if(senderAddress == 0){
        return NEW_DEVICE_ADDR;
    }
    for (i = 0; i < countOfConnectedDevices; i++){
        if(RegisteredDevices[i].deviceAddress == senderAddress){
            return i;
        }
    }
    return DEVISE_NOT_REGISTERED;
}

int tryGetIllumination(uint8_t * buf, int num){
    if(!(RegisteredDevices[num].deviceTypeLo & ILLUMINATION_KPS_3227)){
        return 0;
    }
    RegisteredDevices[num].CurentData.illuminationKPS3227 = buf[5];
}

int tryGetTempFromLM75(uint8_t * buf, int num){
    if(!(RegisteredDevices[num].deviceTypeLo & TEMPERATURE_LM75)){
        return 0;
    }
    RegisteredDevices[num].CurentData.temperatureLM75 = buf[12] + (float) ((uint8_t) buf[13]/32)/10;
    return 1;
}

int tryGetInfoFromSht3(uint8_t * buf, int num){
    if(!(RegisteredDevices[num].deviceTypeLo & TEMPERATURE_AND_HUMIDITY_SHT30)){
        return 0;
    }
    RegisteredDevices[num].CurentData.temperatureSHT30 = sht3GetTemp(buf);
    RegisteredDevices[num].CurentData.humiditySHT30 = sht3GetHumidity(buf);
    return 1;
}

int tryGetStateOfSafetySensors(uint8_t * buf, int num){
    RegisteredDevices[num].CurentData.countOfChangesOnFirstChanel = buf[1];
    RegisteredDevices[num].CurentData.countOfChangesOnSecondChanel = buf[2];
}

int checkSafetyInputs(uint8_t stateByte){
    uint8_t firstInputState, secondInputState, firstMask = 0b00001111, secondMask = 0b11110000;
    firstInputState = stateByte & firstMask;
    secondInputState = (stateByte & secondMask) >> 4;
    switch(firstInputState){
        case STATE_IDLE:
            //IDLE
            return 1;
        break; 
        
        case STATE_SHORT_CIRCUIT:
            //TODO Send message to server short circuit
            return 0;
        break;
        
        case STATE_OPEN:
            //TODO Send message to server wire broken
            return 0;
        break;

        case STATE_CHANGE:
            return 1;
        break;

    }
    switch(secondInputState){
        case STATE_IDLE:
            //IDLE 
            return 1;
        break; 

        case STATE_SHORT_CIRCUIT:
            //TODO Send message to server short circuit
            return 0;
        break;
        
        case STATE_OPEN:
            //TODO Send message to server wire broken
            return 0;
        break;

        case STATE_CHANGE:
            return 1;
        break;
    }
}

void decodeReceivedData(uint8_t * buf, int num){
    
    tryGetInfoFromSht3(buf, num);
    tryGetIllumination(buf, num);
    if(RegisteredDevices[num].deviceSensorsType){//for devices with safety inputs)
        checkSafetyInputs(buf[0]);
        tryGetStateOfSafetySensors(buf, num);
    }

    
}

void checkTemperatureChanges(int num){

    if(RegisteredDevices[num].PreviousData.temperatureSHT30 != RegisteredDevices[num].CurentData.temperatureSHT30){
       
        RegisteredDevices[num].PreviousData.temperatureSHT30 = RegisteredDevices[num].CurentData.temperatureSHT30;
        RegisteredDevices[num].shouldBeSent |= DEFAULT_STATE;
    }

}
void checkHumidityChanges(int num){

    if(RegisteredDevices[num].PreviousData.humiditySHT30 != RegisteredDevices[num].CurentData.humiditySHT30){
       
        RegisteredDevices[num].PreviousData.humiditySHT30 = RegisteredDevices[num].CurentData.humiditySHT30;
        RegisteredDevices[num].shouldBeSent |= DEFAULT_STATE;
    }

}
void checkIlluminationChanges(int num){

    if(RegisteredDevices[num].PreviousData.illuminationKPS3227 != RegisteredDevices[num].CurentData.illuminationKPS3227){
       
        RegisteredDevices[num].PreviousData.illuminationKPS3227 = RegisteredDevices[num].CurentData.illuminationKPS3227;
        RegisteredDevices[num].shouldBeSent |= DEFAULT_STATE;
    }

}
void checkSafetySensorsChanges(int num){
    int firstVol, secondVol;
    char buf[50];
    firstVol = abs(RegisteredDevices[num].PreviousData.countOfChangesOnFirstChanel - RegisteredDevices[num].CurentData.countOfChangesOnFirstChanel);
    if(firstVol > 0){
        RegisteredDevices[num].shouldBeSent |= SAFETY_SATE;
        RegisteredDevices[num].PreviousData.countOfChangesOnFirstChanel = RegisteredDevices[num].CurentData.countOfChangesOnFirstChanel;
        sprintf(buf, "sensor_%d,p_state,%s_%d", num, RegisteredDevices[num].firstSensorType,firstVol);
        sendMessageToDomovoySystem(buf);
    }
    secondVol = abs(RegisteredDevices[num].PreviousData.countOfChangesOnSecondChanel - RegisteredDevices[num].CurentData.countOfChangesOnSecondChanel);
    if(secondVol > 0){
        RegisteredDevices[num].shouldBeSent |= SAFETY_SATE;
        RegisteredDevices[num].PreviousData.countOfChangesOnSecondChanel = RegisteredDevices[num].CurentData.countOfChangesOnSecondChanel;
        sprintf(buf, "sensor_%d,p_state,%s_%d", num, RegisteredDevices[num].secondSensorType,secondVol);
        sendMessageToDomovoySystem(buf);
    }
    
}

void sendDefaultState(int num){
    char buf[30];
    if(!(RegisteredDevices[num].deviceTypeLo & RELAY_OUTPUT)){
        sprintf(buf, "sensor_%d,state,%.1f_%.1f_%d",
        num, RegisteredDevices[num].CurentData.temperatureSHT30, RegisteredDevices[num].CurentData.humiditySHT30, RegisteredDevices[num].CurentData.illuminationKPS3227);
        syslog(LOG_DEBUG, "[STATE] Addr: %x Hum: %f Temp: %f Lum: %d", RegisteredDevices[num].deviceAddress, RegisteredDevices[num].CurentData.humiditySHT30, RegisteredDevices[num].CurentData.temperatureSHT30, RegisteredDevices[num].CurentData.illuminationKPS3227);
        sendMessageToDomovoySystem(buf);
    }
}

void sendInfoToServerAndOwerwriteChanges(int sensorNum){
    checkTemperatureChanges(sensorNum);
    checkHumidityChanges(sensorNum);
    checkIlluminationChanges(sensorNum);
    checkSafetySensorsChanges(sensorNum);
    
    if(RegisteredDevices[sensorNum].shouldBeSent & DEFAULT_STATE){
        sendDefaultState(sensorNum);
    }
    
}

void setDefaultMessage(int num){
    RegisteredDevices[num].NextMessage.command = REQUEST_NOW_STATE;
    memset(RegisteredDevices[num].NextMessage.data, 0, 20);
    RegisteredDevices[num].NextMessage.dataLen = 0;
}


void requestDataAboutSensorFromServer(){
    uint8_t sensorsType;
    char answer[30], buf[30]; 
    sprintf(buf, "new_device,new_id,sensor_%d", countOfConnectedDevices-1);
    sendMessageToDomovoySystem(buf);    
}

void connectToDomovoySystem() {
    char path[512];
    do{
        sleep(5);
        sprintf(path, "%s/%s%s", PIPE_PATH, RGT_WIRED_SENSOR_PIPE, "_mosi");
        domovoy_ws_mosi = open(path, O_RDONLY | O_NONBLOCK);
        if (domovoy_ws_mosi < 0) {
            syslog(LOG_ERR, "Cant open pipe from domovoy subsystem, errno %d", errno);
        }
        sprintf(path, "%s/%s%s", PIPE_PATH, RGT_WIRED_SENSOR_PIPE, "_miso");
        domovoy_ws_miso = open(path, O_WRONLY);
        if (domovoy_ws_miso < 0) {
            syslog(LOG_ERR, "Cant open pipe to domovoy subsystem, errno %d", errno);
        }
    }while(domovoy_ws_miso < 0 || domovoy_ws_mosi < 0);
}

void writeSensorTypeInfoIntoStructure(int num){
    switch(RegisteredDevices[num].deviceSensorsType >> 4){
        case SMOKE_SENSOR:
            strcpy(RegisteredDevices[num].firstSensorType, "smoke");
        break;
        case FLUID_SENSOR:
            strcpy(RegisteredDevices[num].firstSensorType, "fluid");
        break;
        case MOTION_SENSOR:
            strcpy(RegisteredDevices[num].firstSensorType, "motion");
        break;
        case DOOR_SENSOR:
            strcpy(RegisteredDevices[num].firstSensorType, "door");
        break;
    }

    switch(RegisteredDevices[num].deviceSensorsType & 0b00001111){
        case SMOKE_SENSOR:
            strcpy(RegisteredDevices[num].secondSensorType, "smoke");
        break;
        case FLUID_SENSOR:
            strcpy(RegisteredDevices[num].secondSensorType, "fluid");
        break;
        case MOTION_SENSOR:
            strcpy(RegisteredDevices[num].secondSensorType, "motion");
        break;
        case DOOR_SENSOR:
            strcpy(RegisteredDevices[num].secondSensorType, "door");
        break;
    }
}

char * readParam(char * message, char * bufToWrite){
    int i = 0;
    while((message[i] != '_') & (message[i] != ',') && (i != 10)){
        bufToWrite[i] = message[i];
        i++;
    }
    return &message[i];
}

void switchHandler(char * arg, char * buf, int bitOffset){
    switch(arg[1]){
        case 'n':
            buf[0] |= (1 << bitOffset);
            buf[1] |= (1 << bitOffset);
        break;
        case 'f':
            buf[0] |= (1 << bitOffset);
            buf[1] &= ~(1 << bitOffset);
        break;
        case 'd':
            buf[0] &= ~(1 << bitOffset);
            buf[1] &= ~(1 << bitOffset);
        break;
    }
}


void * receiveMessageFromDomovoySystem( void * arg){
    struct pollfd domovoyFds[1];
    int len;
    char message[50], * msgpntr;
    MessageFromServer Parsed;

    domovoyFds[0].fd = domovoy_ws_mosi;
    domovoyFds[0].events = POLLIN;
    while(1){
        if(len = read(domovoy_ws_mosi, message, 50) > 5/*poll(domovoyFds, 0, -1)*/){
            //syslog(LOG_DEBUG, "[PIPE] Reading message");
            len = read(domovoy_ws_mosi, message, 50);
            if(len < 0){
               //syslog(LOG_ERR, "[PIPE] Can`t read fom pipe. Errno %d", errno);
            }
            syslog(LOG_DEBUG, "[PIPE] Was read  %s", message);
            msgpntr = readParam(message, Parsed.type);
            Parsed.number = strtol(msgpntr + 1, &msgpntr, 10);
            msgpntr = readParam(msgpntr + 1, Parsed.func);
            msgpntr = readParam(msgpntr + 3, Parsed.fitstArg);
            msgpntr = readParam(msgpntr + 3, Parsed.secondArg);
            //syslog(LOG_DEBUG, "[PIPE] \nmsg type: %s\nmsg func: %s\nfirst arg: %s\nsecond arg: %s", Parsed.type, Parsed.func, Parsed.fitstArg, Parsed.secondArg); 
            char buf[2];
            if(!strncmp(Parsed.func, "switch", 6)){
                switchHandler(Parsed.fitstArg, buf, 3);
                switchHandler(Parsed.secondArg, buf, 4);                
                syslog(LOG_DEBUG, "[PIPE] should be sent %x %x", buf[0], buf[1]);
                setNewMessage(COMMAND_NEW_PARAM, buf, 2, Parsed.number);
            }else if(!strncmp(Parsed.func, "info", 4)){
                syslog(LOG_DEBUG, "[PIPE] Received safety sensors type");
                strcpy(RegisteredDevices[Parsed.number].firstSensorType, Parsed.fitstArg);
                strcpy(RegisteredDevices[Parsed.number].secondSensorType, Parsed.secondArg);
                RegisteredDevices[Parsed.number].deviceSensorsType = (checkSafetySensorsType(Parsed.fitstArg) << 4) | checkSafetySensorsType(Parsed.secondArg);
                setNewMessage(COMMAND_SET_DEVICE_TYPE, &RegisteredDevices[Parsed.number].deviceSensorsType, 1, Parsed.number);
                syslog(LOG_DEBUG, "[PIPE] Should be sent %x", RegisteredDevices[Parsed.number].deviceSensorsType);
            }
                      
        }
    }
    
}


void * messageHandler( void * arg){
    int senderNumber, i;
    uint8_t message[30], buf[20];
    while(1){
        //syslog(LOG_DEBUG, "[HANDLER] Waiting for messages");
        sem_wait(&handlerSemaphore);
        //syslog(LOG_DEBUG, "[HANDLER] Working on new message");
        pthread_mutex_lock(&globalBufMutex);
        for (i = 0; i < 30; i++){
            message[i] = globalBuf[i];
        }
        pthread_mutex_unlock(&globalBufMutex);
        //syslog(LOG_DEBUG, "[HANDLER] Message was taken");
        senderNumber = identifyDeviceByAddress(message[3]);
        //syslog(LOG_DEBUG, "[HANDLER] Message from device num %d with addr %d", senderNumber, message[3]);
        switch(message[4]){
            case RESPONSE_COMMAND_NOT_DONE_ERROR:
                //TODO send msg to server that dev don`t handeled command
            break;
            
            case RESPONSE_DEVISE_WITHOUT_ADDRESS:
                buf[0] = chooseNewAddress();
                RegisteredDevices[countOfConnectedDevices].deviceAddress = buf[0];
                syslog(LOG_DEBUG, "[HANDLER] Sendind new addr to device");
                sendMessageToSensor(NEW_DEVICE_ADDR, COMMAND_SET_NEW_ADDRESS,buf, 1, COMMON_TIMEOUT);
                RegisteredDevices[countOfConnectedDevices].registered = NOT_REGISTERED;
                countOfConnectedDevices++;
                
            break;

            case RESPONSE_FACTORY_PARAM:
               // syslog(LOG_DEBUG, "[HANDLER] Received factory param msg");
                sortFactoryParam(message+7,message[6], senderNumber);
                if(!RegisteredDevices[senderNumber].registered){
                    if (RegisteredDevices[senderNumber].deviceTypeLo & (1<<4)){
                        sprintf(buf, "new_device,new_id,sensor_%d", senderNumber);
                        sendMessageToDomovoySystem(buf);
                    }else{
                        sprintf(buf, "new_device,new_id,relay_%d", senderNumber);
                        sendMessageToDomovoySystem(buf);                    
                    }
                }
                setDefaultMessage(senderNumber);
                
            break;

            case RESPONSE_NEW_ADDRESS:
                setNewMessage(REQUEST_FACTORY_PARAM, NULL, 0, senderNumber);
                saveDeviceData(&RegisteredDevices[senderNumber]);
            break;
            
            case RESPONSE_NEW_PARAM_ANSWER:
                //TODO specific magic
                if(RegisteredDevices[senderNumber].deviceTypeLo & RELAY_OUTPUT){
                    switch(message[7] & RELAY_ANSWER_MASK){

                        case FIRST_ERR:
                            //TODO Send msg to server
                        break;

                        case SECOND_ERR:
                            //TODO Send msg to server
                        break;

                        case BOTH_ERR:
                            //TODO Send msg to server
                        break;

                        case BOTH_OK:
                            //Send msg that everything og
                        break;

                    }
                                       
                }
                setDefaultMessage(senderNumber);
            break;

            case RESPONSE_NEW_PARAM_RESPONSE:
                //TODO send msg to server that new param to dev was set
                if(RegisteredDevices[senderNumber].deviceTypeLo & RELAY_OUTPUT){
                    //Everything ok!
                    setDefaultMessage(senderNumber);
                }
            break;

            case RESPONSE_CURRENT_STATE:
                //syslog(LOG_DEBUG, "[HANDLER] Received current state of sensor");
                decodeReceivedData(message + 7, senderNumber);
                sendInfoToServerAndOwerwriteChanges(senderNumber);
                
            break;

            case RESPONSE_RESET:
                //TODO send msg to server that dev was reseted
            break;

            case RESPONSE_SAVE_ADC_VALUE:
                setDefaultMessage(senderNumber);
            break;

            case RESPONSE_SET_DEVICE_TYPE:
                setNewMessage(COMMAND_SAVE_ADC_VALUE, NULL, 0, senderNumber);
            break;
            
            case RESPONSE_READ_EEPROM:
                
            break;

        }
    }
}

void * uartReader( void * arg){
    struct pollfd uartFds[1];
    uint8_t dataBuf[30], symbol, hexBuf[40];
    int messageLenght;

    uartFds[0].fd = uartFd;
    uartFds[0].events = POLLIN;

    while(1) {
        if(poll(uartFds, 1, -1)){
        readFromUart(uartFd, &symbol, 1);
            if(symbol == 0xAA){
                messageLenght = readFullMessage(uartFd, dataBuf);
                if(!checkMessage(dataBuf, messageLenght)){
                    binaryToHexStr(dataBuf, messageLenght, hexBuf);
                    syslog(LOG_DEBUG, "[RECIEVER] Was read incorrect message %s", hexBuf);
                }else{
                    binaryToHexStr(dataBuf, messageLenght, hexBuf);
                    syslog(LOG_DEBUG, "[RECIEVER] Was read %s", hexBuf);
                    int i;
                    pthread_mutex_lock(&globalBufMutex);
                    //syslog(LOG_DEBUG, "[RECIEVER] Mutex locked");
                    for(i = 0; i < messageLenght; i++){
                        globalBuf[i] = dataBuf[i];
                    }
                    pthread_mutex_unlock(&globalBufMutex);
                    //syslog(LOG_DEBUG, "[RECIEVER] Mutex unlocked");
                    sem_post(&handlerSemaphore);
                }   
                sem_post(&uartSemaphore);           
            }
        }    
    }
}

void * theThreadThatShouldAskSensorsTillItsEnd(void * arg){
    struct timespec PreviousTime;
    struct timespec CurrentTime;
    clock_gettime(1, &PreviousTime);
    readSavedDeviceData();
    int requestSpeed, i;
    requestSpeed = readFromSettings(REQUEST_SPEED);
    while(1){
        sleep(3);
        for(i = 0; i < countOfConnectedDevices; i++){
            //syslog(LOG_DEBUG, "[SENDER] %d Should be send %x to %x", i, RegisteredDevices[i].NextMessage.command, RegisteredDevices[i].deviceAddress);
            if(sendMessageToSensor(RegisteredDevices[i].deviceAddress, 
                RegisteredDevices[i].NextMessage.command, 
                RegisteredDevices[i].NextMessage.data, 
                RegisteredDevices[i].NextMessage.dataLen, COMMON_TIMEOUT))
                {
                RegisteredDevices[i].countOfErrorsInMessagesFromDevice++;
                if(RegisteredDevices[i].countOfErrorsInMessagesFromDevice >= 20){
                    //syslog(LOG_ERR, "[SENDER] UART Timeout. Receiver address : %x | Errno %d", RegisteredDevices[i].deviceAddress, errno);
                    RegisteredDevices[i].countOfErrorsInMessagesFromDevice = 0;
                }
            }
            //syslog(LOG_DEBUG, "[SENDER]Was sent");
            usleep(2500);
        }
        clock_gettime(0, &CurrentTime);
        if(CurrentTime.tv_sec - PreviousTime.tv_sec >= 15){
           //syslog(LOG_DEBUG, "[SENDER] Looking for new device");
            if(sendMessageToSensor(NEW_DEVICE_ADDR, REQUEST_FOR_DEVICES_WITHOUT_ADDRESS, NULL, 0, COMMON_TIMEOUT))
            {
              //  syslog(LOG_ERR, "[SENDER] Nobody on line");
            }
            PreviousTime.tv_sec = CurrentTime.tv_sec;
        }
    }
}

void main(){

    pthread_t wsRd, wsWr, wsH, dmRd;
    int rez;

    openlog("wired_sensors", 0, LOG_LOCAL0);

    connectToDomovoySystem();

    sem_init(&handlerSemaphore, 0, 0);    
    sem_init(&uartSemaphore, 0, 0);

    syslog(LOG_DEBUG, "Trying to init pin 3");
    pinFd = pinInit(3, PIN_OUTPUT);
    if(pinFd < 0){
        syslog(LOG_DEBUG, "Can`t init pin 3. Errno: %d", errno);
    }else{
        syslog(LOG_DEBUG, "Pin 3 was succesfully initialized");
    }

    pthread_mutex_init(&globalBufMutex, NULL);
    
    uartFd = open(RS485_PORT, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if(uartFd <= 0)
    {
        syslog(LOG_DEBUG,"Can't open uart port (%s), Error: %d", RS485_PORT, errno);
        exit(0);
    }

    struct termios oldtio,newtio;


    tcgetattr(uartFd,&oldtio); /* save current port settings */

    bzero(&newtio, sizeof(newtio));
    newtio.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;

    /* set input mode (non-canonical, no echo,...) */
    newtio.c_lflag = 0;

    newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
    newtio.c_cc[VMIN]     = 1;   /* bprintf("Message to send:\nlocking read until 5 chars received */

    tcflush(uartFd, TCIFLUSH);
    tcsetattr(uartFd,TCSANOW,&newtio);

    fds[0].fd = domovoy_ws_mosi;
    fds[0].events = POLLIN;

    syslog(LOG_DEBUG, "Starting up uart reader");
    rez = pthread_create(&wsRd, NULL, uartReader, NULL);
    if (rez) {
         syslog(LOG_ERR, "Can't create uart reader thread");
         exit(1);
    }
    syslog(LOG_DEBUG, "Uart reader was started");

    syslog(LOG_DEBUG, "Starting up uart sender");
    rez = pthread_create(&wsWr, NULL, theThreadThatShouldAskSensorsTillItsEnd, NULL);
    if (rez) {
         syslog(LOG_ERR, "Can't create message sender thread");
         exit(1);
    }
    syslog(LOG_DEBUG, "Uart sender was started");

    syslog(LOG_DEBUG, "Starting up message handler");
    rez = pthread_create(&wsH, NULL, messageHandler, NULL);
    if (rez) {
         syslog(LOG_ERR, "Can't create message handler thread");
         exit(1);
    }
    syslog(LOG_DEBUG, "Message handler was started");

    syslog(LOG_DEBUG, "Starting up message reseiver");
    rez = pthread_create(&dmRd, NULL, receiveMessageFromDomovoySystem, NULL);
    if (rez) {
         syslog(LOG_ERR, "Can't start messages receiver from domovoy");
         exit(1);
    }
    syslog(LOG_DEBUG, "Message reseiver was started");

    pthread_join(wsRd, NULL);
}