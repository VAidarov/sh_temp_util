#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define GPIO_PATH "/sys/class/gpio/"
#define GPIO_VAl "gpio%d/value"
#define GPIO_DIR "gpio%d/direction"
#define GPIO_INIT "export"
#define GPIO_OFF "unexport"

#define PIN_OUTPUT 1
#define PIN_INPUT 0
#define HIGH 1
#define LOW 0

int pinInit(int pin, int direction);

int pinState(int pin, int state, int fd);

void pinOff(int pin);
