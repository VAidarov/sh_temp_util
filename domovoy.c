#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <poll.h>
#include <syslog.h>

#include "domovoy_conf.h"
#include "domovoy.h"
#include "hiredis/hiredis.h"
#include "wired_sensors.h"

redisContext *r_context1, *r_context2;
pthread_mutex_t nooLiteMutex, rgtWsMutex, ledMutex, globalBufMutex;
int rgt_ws_mosi = -1, 
    rgt_ws_miso = -1, 
    noo_mosi = -1, 
    noo_miso = -1, 
    led_mosi = -1;

int connectToSmartHomeServer() {
    const char *hostname = "127.0.0.1";
    int port = 6379;

    r_context1 = redisConnect(hostname, port);
    if (r_context1 == NULL || r_context1->err) {
        if (r_context1) {
            char err[MAX_MESSAGE_LENGTH];
            sprintf(err, "Connection error: %s\n", r_context1->errstr);
            syslog(LOG_ERR, err);
            redisFree(r_context1);
        } else {
            syslog(LOG_ERR, "Connection error: can't allocate redis context\n");
        }
        return 1;
    }
    r_context2 = redisConnect(hostname, port);
    if (r_context2 == NULL || r_context2->err) {
        if (r_context2) {
            char err[MAX_MESSAGE_LENGTH];
            sprintf(err, "Connection error: %s\n", r_context2->errstr);
            syslog(LOG_ERR, err);
            redisFree(r_context2);
        } else {
            syslog(LOG_ERR, "Connection error: can't allocate redis context\n");
        }
        return 1;
    }
    return 0;
}

int sendMessageToSmartHomeServer(char * msg) {
    redisReply *reply = redisCommand(r_context1, "LPUSH %s %s", MESSAGE_QUEUE_NAME, msg);    
    if(reply->type == REDIS_REPLY_ERROR)
    {
        syslog(LOG_ERR, reply->str);
    }
    freeReplyObject(reply);
    return 0;
}

int receiveCommandFromSmartHomeServer(char * cmd) {
    
    redisReply *reply = redisCommand(r_context2, "BRPOP %s 1", COMMAND_QUEUE_NAME);
    if(reply->type == REDIS_REPLY_ERROR)   {
        syslog(LOG_ERR, reply->str);
    }
    else if(reply->type == REDIS_REPLY_ARRAY) {
        strncpy(cmd, reply->element[1]->str, MAX_MESSAGE_LENGTH);
    }
    freeReplyObject(reply);
    return 0;
}


void connectToRGTWiredSensors() {
    if(mkdir(PIPE_PATH, S_IRWXU | S_IRWXG | S_IRWXO )){
        if(errno != EEXIST){
            syslog(LOG_ERR, "Can't create pipe dir");
            exit(1);
        }
    }
    char path[512];
    sprintf(path, "%s/%s%s", PIPE_PATH, RGT_WIRED_SENSOR_PIPE, "_mosi");
    if(mkfifo(path, S_IRWXU | S_IRWXG | S_IRWXO )) {
        if(errno != EEXIST){
            syslog(LOG_ERR, "Can't create pipe to wired sensors");
            exit(1);
        }
    }
    rgt_ws_mosi = open(path, O_WRONLY);
    if (rgt_ws_mosi < 0) {
        syslog(LOG_ERR, "Cant open pipe to wired sensor");
        exit(1);
    }
    sprintf(path, "%s/%s%s", PIPE_PATH, RGT_WIRED_SENSOR_PIPE, "_miso");
    if(mkfifo(path, S_IRWXU | S_IRWXG | S_IRWXO )) {
        if(errno != EEXIST){
            syslog(LOG_ERR, "Can't create pipe from wired sensors");
            exit(1);
        }
    }
    rgt_ws_miso = open(path, O_RDONLY | O_NONBLOCK);
    if (rgt_ws_mosi < 0) {
        syslog(LOG_ERR, "Cant open pipe from wired sensor");
        exit(1);
    }
}

void sendMessageToRGTWiredSensors(char * msg, int msgLen) {
    int len = write(rgt_ws_mosi, msg, msgLen);
    if(len < 0){
        syslog(LOG_ERR,"Can`t write into WS pipe");
    }
}

int receiveMessageFromRGTWiredSensors() {
    int len;
    char message[30];
    len = read(rgt_ws_miso, message, 30);
    if(len < 0){
        syslog(LOG_ERR, "Can`t read from WS pipe");
    }
    sendMessageToSmartHomeServer(message);
}
 

void connectToNooLiteSubsystem() {
    syslog(LOG_DEBUG, "Creating fifo's directory");
    if(mkdir(PIPE_PATH, S_IRWXU | S_IRWXG | S_IRWXO )){
        if(errno != EEXIST){
            syslog(LOG_ERR, "Can't create pipe dir");
            exit(1);
        }
    }
    char path[512];

    syslog(LOG_DEBUG, "Creating fifo noolite_mosi");
    sprintf(path, "%s/%s%s", PIPE_PATH, NOOLITE_PIPE, "_mosi");
    if(mkfifo(path, S_IRWXU | S_IRWXG | S_IRWXO )) {
        if(errno != EEXIST){
            syslog(LOG_ERR, "Can't create pipe to NooLite subsystem");
            exit(1);
        }
    }

    syslog(LOG_DEBUG, "Open fifo noolite_mosi");
    noo_mosi = open(path, O_WRONLY);
    if (noo_mosi < 0) {
        syslog(LOG_ERR, "Cant open pipe to NooLite subsystem");
        exit(1);
    }


    syslog(LOG_DEBUG, "Creating fifo noolite_miso");
    sprintf(path, "%s/%s%s", PIPE_PATH, NOOLITE_PIPE, "_miso");
    if(mkfifo(path, S_IRWXU | S_IRWXG | S_IRWXO )) {
        if(errno != EEXIST){
            syslog(LOG_ERR, "Can't create pipe from NooLite subsystem");
            exit(1);
        }
    }

    syslog(LOG_DEBUG, "Open fifo noolite_miso");
    noo_miso = open(path, O_RDONLY | O_NONBLOCK);
    if (noo_mosi < 0) {
        syslog(LOG_ERR, "Cant open pipe from NooLite subsystem");
        exit(1);
    }
}

void sendMessageToNooLiteSubsystem(uint8_t * msg) {
    pthread_mutex_lock(&nooLiteMutex);
    write(noo_mosi, msg, msg[0] + 1);
    pthread_mutex_unlock(&nooLiteMutex);
}

void receiveMessageFromNooLiteSubsystem() {
    uint8_t c;
    uint8_t buf[MAX_MESSAGE_LENGTH];
    char msg[MAX_MESSAGE_LENGTH];
    syslog(LOG_DEBUG, "Message Recieved");
    if(read(noo_miso, &c, 1)){
        if(read(noo_miso, buf, c) == c) {   syslog(LOG_DEBUG, "Message from noolite recieved");
            switch (buf[0]){
                case MESSAGE_TYPE_INFO:
                    switch(buf[2]){
                        case 0:
                        case 2:
                            sprintf(msg, "light_%d,state,%d\0", buf[3], buf[2] >> 1);
                            sendMessageToSmartHomeServer(msg);  syslog(LOG_DEBUG, "zog");
                            break;
                        default:
                            syslog(LOG_ERR, "Unexpected formated message from NooLite");
                            break;
                    }

                    break;
                default:
                    syslog(LOG_ERR, "Unexpected type message from NooLite");
                    return;
            }
        }
        else {
            syslog(LOG_ERR, "Wrong formated message from NooLite Subsystem");
        }
    }

}
 

void * upward_data_processing (void *arg) {
    struct pollfd fds[2];
    fds[0].fd = noo_miso;
    fds[0].events =  POLLIN;
    fds[1].fd = rgt_ws_miso;
    fds[1].events =  POLLIN;
    while(1) {
        if(poll(fds, 2, -1) > 0) {
            if ((fds[0].revents) & POLLIN) {
                receiveMessageFromNooLiteSubsystem();
            }
            if ((fds[1].revents) & POLLIN) { 
                receiveMessageFromRGTWiredSensors();
            }
        }

    }
}

void process_command_to_noolite(char * cmd){
    uint8_t msg[MAX_MESSAGE_LENGTH];
    if(strstr(cmd + 8, "state") != NULL){
        msg[0] = 3;
        msg[1] = MESSAGE_TYPE_ACTION;
        msg[2] = (atoi(strstr(cmd + 8, "state")+ 6) << 1);
        msg[3] = atoi(cmd + 6);  
    }
    sendMessageToNooLiteSubsystem(msg);
}

void * top_down_data_processing (void *arg){
    char * buf = malloc(MAX_MESSAGE_LENGTH);
    
    while(1) {
        receiveCommandFromSmartHomeServer(buf);
        if (strstr(buf, "light_") == buf){
            process_command_to_noolite(buf);
        }
        if(strstr(buf, "sensor_") || strstr(buf, "relay_")){
            sendMessageToRGTWiredSensors(buf, strlen(buf));
            syslog(LOG_DEBUG, "[REDIS PROCESSING] Was sent to wired sensors: %s", buf);
        }
        buf[0] = 0;
    }
}


void set_noolite_uart_port (char * path) {
    uint8_t msg[MAX_MESSAGE_LENGTH];
    msg[1] = MESSAGE_TYPE_CONFIG;
    msg[2] = MESSAGE_TYPE_CONFIG_TTY_ADRESS;
    strcpy(msg + 3, path);
    msg[0] = strlen(path) + 3;
    sendMessageToNooLiteSubsystem(msg);
}



int main(int argc, char **argv) {
    openlog("Domovoy", 0 , LOG_LOCAL0);
    syslog(LOG_DEBUG, "Starting domovoy system");
    
   // start_log(LOG_FOLDER, LOG_FILE, ERROR_LOG_FILE);
    syslog(LOG_DEBUG, "Connecting to redis db");
    int ernum = 0; 
    while(ernum < 10){
        if(!connectToSmartHomeServer()){
            break;
        }
        sleep(1);
    }
    syslog(LOG_DEBUG, "Ok!");
    
    syslog(LOG_DEBUG, "Connecting to noolite");
    connectToNooLiteSubsystem();
    syslog(LOG_DEBUG, "Connected!");

    syslog(LOG_DEBUG, "Connecting to wired sensors");
    connectToRGTWiredSensors();
    syslog(LOG_DEBUG, "Connected!");


    
    pthread_mutex_init(&nooLiteMutex, NULL);    
    
    syslog(LOG_DEBUG, "Setting noolite port");    
    set_noolite_uart_port(NOOLITE_PORT);
    pthread_t up, down;

    syslog(LOG_DEBUG, "Starting down->up thread");
    
    int rez = pthread_create(&up, NULL, upward_data_processing, NULL);
    if (rez) {
         syslog(LOG_ERR, "Can't create thread");
         exit(1);
    }

    syslog(LOG_DEBUG, "Starting up->down thread");
    
    rez = pthread_create(&down, NULL, top_down_data_processing, NULL);
    if (rez) {
         syslog(LOG_ERR, "Can't create thread");
         exit(1);
    }

    pthread_join(up, NULL);


    
 
 }    
    
     
