


if [ ! -d "hiredis" ]
then
 git clone https://github.com/redis/hiredis.git
fi

cd hiredis
if [ ! -f "hiredis.o" ] 
then
make hiredis.o
fi

if [ ! -f "sds.o" ]
then 
make sds.o
fi
if [ ! -f "read.o" ]
then 
make read.o
fi
if [ ! -f "net.o" ] 
then
make net.o
fi
cd ..
