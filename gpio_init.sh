#!/bin/sh

# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.

#if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
#    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
#fi

### BEGIN INIT INFO
# Provides:          gpio_init.sh
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: PA3 init script
# Description:       This file should be used to construct scripts to be
#                    placed in /etc/init.d.  This example start a
#                    single forking daemon capable of writing a pid
#                    file.  To get other behavoirs, implemend
#                    do_start(), do_stop() or other functions to
#                    override the defaults in /lib/init/init-d-script.
### END INIT INFO

# Author: Aron <v.aydarov@rugadget.ru>


DESC="PA3 init script"
            start(){
                echo 3 > /sys/class/gpio/export
                echo "out" > /sys/class/gpio/gpio3/direction
                chmod +w /sys/class/gpio/gpio3/value
                exit 0;
            }

            stop(){
                echo 0 > /sys/class/gpio/gpio3/value
                echo "in" > /sys/class/gpio/gpio3/direction
                echo 3 > /sys/class/gpio/unexport
                exit 0;
            }

            case "$1" in 
                start)
                    start
                    ;;
                stop)
                    stop
                    ;;
            esac 