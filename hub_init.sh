#!/bin/sh

# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.

#if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
#    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
#fi

### BEGIN INIT INFO
# Provides:          hub_init.sh
# Required-Start:    $all
# Required-Stop:     $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start up smart home hub environment
# Description:       This script is used for deployment of smart home environment
#                    Nouthing more and nouthing less
### END INIT INFO

# Author: Aron <v.aydarov@rugadget.ru>


DESC="Smart Home hub environment"
NOO="noolite"
DOM="domovoy"
WS="wired_sensors"
            start(){
                $DOM &
                sleep 1
                $NOO &
                sleep 1
                $WS &
                exit 0;
            }

            stop(){

                pkill $DOM
                pkill $NOO
                pkill $WS
                exit 0;
            }

            case "$1" in 
                start)
                    start
                    ;;
                stop)
                    stop
                    ;;
            esac 