#define RGT_WIRED_SENSOR_PORT "/dev/ttyS2"
#define NOOLITE_PORT "/dev/ttyS1"



#define COMMAND_QUEUE_NAME "rgt_sh_commands"
#define MESSAGE_QUEUE_NAME "rgt_sh_messages"


#define MAX_MESSAGE_LENGTH 256





int connectToSmartHomeServer();
int sendMessageToSmartHomeServer(char * msg);
int receiveCommandFromSmartHomeServer(char * cmd);



void connectToRGTWiredSensors();
void sendMessageToRGTWiredSensors(char * msg, int len);

 

void connectToNooLiteSubsystem();
 
