#include "sht3.h"


float sht3GetTemp(uint8_t * buf){
    return -47 + 175 *(float) ( (int)(buf[8] << 8) + buf[9])/ 0xFFFF;  
}

float sht3GetHumidity(uint8_t * buf){
    return  100 * (float)( (int)(buf[10] << 8) + buf[11])/ 0xFFFF;      
}
